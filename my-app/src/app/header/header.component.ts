import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  styles: [
    `
    div{
      background: lightgray;
      padding: 20px;
    }
    `
  ]
})
export class HeaderComponent {

  @Input()
  uname:string;

}
