import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http:HttpClient,
    ) { }

  register(employee) {
    return this.http.post('http://localhost:8080/register', employee);
  }
  
  registerEmp(employee) {
    return this.http.post('http://localhost:8080/PMS/pub/registerEmp', employee);
  }

  getEmployees() {
    return this.http.get("https://jsonplaceholder.typicode.com/users");
  }

  getAllEmps() {
    return this.http.get("http://localhost:8080/PMS/pub/emps");
  }

}
