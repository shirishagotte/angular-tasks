import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';


/**
 * @Injectable() decorator means we can inject this service in any component
 * All services in angular must be registered in providers[] 
 */
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) { }

  getAllUsers(){
    return [
      {name: 'Hari', empid:1001},
      {name: 'Arun', empid:1002},
      {name: 'Kavya', empid:1003}    
    ]
  }

  getAllUsersFromApi(){
    return this.http.get("https://jsonplaceholder.typicode.com/users");
  }

  getAllUsersServiceFail(){
    return this.http.get("https://jsonplaceholder.typicode.com/userss");
  }

  /*
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        errorMessage = 'Error: ' + error.error;
    } else {
        errorMessage = 'Error Code : ' + error.status + '\nmessage: ' + error.error;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  */
}
