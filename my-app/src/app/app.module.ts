import { ShoppingEffects } from './store/effects/shopping-effects';
import { RouterModule, Routes } from '@angular/router';
import { UsersService } from './services/users.service';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { ViewModule } from './view/view.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ChildComponent } from './child/child.component';
import { CustomDirectiveDirective } from './custom-directive.directive';
import { BindingComponent } from './binding/binding.component';
import { HttpClientModule} from '@angular/common/http';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { UsersComponent } from './users/users.component';
import { TableFilterPipe } from './table-filter.pipe';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeFilterPipe } from './_pipes/employee-filter.pipe';
import { RegistrationComponent } from './registration/registration.component'
import { ShoppingReducer } from './store/reducers/shopping-reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';

const routes:Routes = [
  {path:'', redirectTo:'employees', pathMatch:'full'},
  {path:'users', component:UsersComponent},
  {path:'about', component:AboutComponent},
  {path:'contact', component:ContactComponent},
  {path:'employees', component:EmployeesComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ChildComponent,
    CustomDirectiveDirective,
    BindingComponent,
    AboutComponent,
    ContactComponent,
    UsersComponent,
    TableFilterPipe,
    EmployeesComponent,
    EmployeeFilterPipe,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ViewModule, 
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot({
      shopping: ShoppingReducer
    }),
    EffectsModule.forRoot([ShoppingEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
