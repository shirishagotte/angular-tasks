import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'employeeFilter'
})
export class EmployeeFilterPipe implements PipeTransform {

   transform(employees: any[], searchEmployee: string){

    if (!employees || !searchEmployee) {
      return employees;
    }

    employees = employees.filter((emp) => {
      return (emp.name).toLowerCase().indexOf(searchEmployee.toLowerCase()) !== -1 ||
      (emp.id).toString().indexOf(searchEmployee) !== -1;
    });
    
    return employees;
  }

}
