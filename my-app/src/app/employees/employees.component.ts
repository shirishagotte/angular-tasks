import { Course } from './../course';
import { Employee } from './../employee';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit{
  submitButtonStatus:boolean=false;
  courseSelected=1;

  sortType: string;
  sortReverse: boolean = false;

  selectedEmployees:Employee[]=[];
  originalEmployees:Employee[]=[];
  copyOfEmployees: Employee[]=[];

  ngOnInit(){
    // this.originalEmployees = [
    //   {id : 101 , name : 'Ajay', selected : false, courseSelected : this.courseSelected},
    //   {id : 102 , name : 'Geetha' , selected : false, courseSelected : this.courseSelected},
    //   {id : 103 , name : 'Sunny' , selected : false, courseSelected : this.courseSelected},
    //   {id : 104 , name :  'Akhila' , selected : false, courseSelected : this.courseSelected},
    //   {id : 105 , name : 'Jaya' , selected : false, courseSelected : this.courseSelected}
    // ];

    this.originalEmployees = [
      {id : 101 , name : 'Ajay', selected : false, courseSelected : "Java"},
      {id : 102 , name : 'Geetha' , selected : false, courseSelected : "Java"},
      {id : 103 , name : 'Sunny' , selected : false, courseSelected : "Java"},
      {id : 104 , name :  'Akhila' , selected : false, courseSelected : "Java"},
      {id : 105 , name : 'Jaya' , selected : false, courseSelected : "Java"}
    ];


    this.copyOfEmployees=this.originalEmployees.concat(); //combining 2 arrays, additional items added to end of array1

    // this.copyOfEmployees = this.copyOfEmployees.map((employee) => {
    //   employee.selected = false;
    //   return employee;
    // });

    this.selectedEmployees=[];
  }
    
  // employees:any[]=[
  //   {name:'Ajay', id:'101', courses: ['Java', 'Angular']},
  //   {name:'Geetha', id:'102', courses: ['Java', 'Oracle']},
  //   {name:'Sunny', id:'103', courses: ['Oracle']},
  //   {name:'Akhila', id:'104', courses: ['Java', 'Dot Net']},
  //   {name:'Jaya', id:'105', courses: ['Dot Net']}
  // ];
  
  /*
  Initialize arrays in a constructor
  Create new array copy
  Add a new property for selected=false

  When the dropdown option chosen, then in new copy, for the particular emp id, we need add this selected course value
  */
 
  courses:Course[]=[
    {id : 1 , name : 'Java'},
    {id : 2 , name : 'Dot Net'},
    {id : 3 , name : 'Oracle'}
  ];

  selectedCourses:any[]=[];
  /*
  create duplicate array copy from original array
  */
  deleteRow(id){
    for(let i = 0; i < this.copyOfEmployees.length; ++i){
        if (this.copyOfEmployees[i].id === id) {
            this.copyOfEmployees.splice(i,1);
        }
    }
  }

  sort(value) {
    this.sortType = value;
    this.sortReverse = !this.sortReverse;
    this.copyOfEmployees.sort(this.dynamicSort(value));
  } 

  dynamicSort(value) {
    let sortOrder = -1;

    if (this.sortReverse)
        sortOrder = 1;

    return function (a, b) {
      let result = (a[value] < b[value]) ? -1 : (a[value] > b[value]) ? 1 : 0;
      return result * sortOrder;
    }
  }

  selected(emp){
    this.selectedEmployees.indexOf(emp)==-1 ? this.selectedEmployees.push(emp):this.selectedEmployees.splice(this.selectedEmployees.indexOf(emp),1);
    this.submitButtonStatus=false;
  }
  
  onSubmit(){
    this.submitButtonStatus=true;
  }

  onReset(){
    this.ngOnInit();
  }

  //When the dropdown option chosen, then in new copy, for the particular emp id, we need add this selected course value
  // selectedOption(emp){  
  //   console.log("course selected for "+emp.name+" is "+emp.courseSelected);
  //   this.copyOfEmployees.filter(e=>{
  //     if(e.id==emp.id){
  //        this.courses.filter(c=>{
  //         if(c.id==emp.courseSelected){
  //           this.selectedCourses.push({eid:emp.id, cname:c.name});
  //           this.selectedEmployees = this.selectedEmployees.map((employee) => {
  //             employee.courseSelected = c.name;
  //             return employee;
  //           });
  //         }
  //        }); 
  //       }
  //   });    
  // }
}
