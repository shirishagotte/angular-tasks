import { logging } from 'protractor';
import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {

  @Output()
  notify:EventEmitter<string> = new EventEmitter<string>();

  passData(){
    this.notify.emit("Data coming from child component");
  }

  @Input()
  uname:string;

}
