import { Employee } from './../employee';
import { UsersService } from './../services/users.service';
import { RegisterService } from './../services/register.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  
  registrationForm: FormGroup;
  users: any[];
  errorMessage:string='';
  emps:any[];

  constructor(
    private fb: FormBuilder,
    private registerService: RegisterService,
    private usersService:UsersService
    ) {}

  ngOnInit() {
    this.getAllEmps();
    this.registrationForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      emailId: ['', Validators.required],
      gender: ['', Validators.required]
    });
  }

  onSubmit(): void{
    console.log(this.registrationForm.value);
    this.registerService.register(this.registrationForm.value).subscribe(()=>{
      console.log("Registration Success...");
    });
  }
  onEmpSubmit(): void{
    console.log(this.registrationForm.value);
    this.registerService.registerEmp(this.registrationForm.value).subscribe(()=>{
      console.log("Registration Success...");
    },
    (err)=>{
      console.log("Error : " +err);
    }
    );
  }
  getEmployee(){
    this.usersService.getAllUsersFromApi().subscribe((data:any[])=>{
      this.users = data;
    },
    (err)=>{
      this.errorMessage=err.message;
    }
    );
  }

  getAllEmps(){
    this.registerService.getAllEmps().subscribe((data:any[])=>{
      this.emps = data;
    },
    (err)=>{
      this.errorMessage=err.message;
    }
    );
  }

}

//https://embed.plnkr.co/UDYDp0gXx5H9OhmMKicL/