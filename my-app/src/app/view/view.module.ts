import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewComponent } from './view/view.component';

@NgModule({
  declarations: [ViewComponent],
  imports: [
    CommonModule
  ],
  exports:[
    ViewComponent
  ]
})
/**
 * We can generate another module by using-
 * ng generate module module-name OR ng g m module-name
 * 
 * This class is also annotated with @NgModule() decorator
 * This informs angular that this class is also a module
 * 
 * We can also create components in this newly created module
 * The components created in this module will be available to each other,
 * By importing and adding the components in declarations[] of the ViewModule
 * 
 * However, these components will not be available to the components outside this module by default
 * We need to export the components in our module by adding the components in the exports[] of @NgModule()
 *Then, other modules can import this module and access it components
 */
export class ViewModule { }
