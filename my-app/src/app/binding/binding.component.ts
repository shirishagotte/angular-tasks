import { Component } from '@angular/core';

/**
 * Interpolation {{}} provides data binding from component to view, it is one way data binding
 * Property Binding [] allows us to bind property of a view element to the value of the template expression
 * Event Binding () is used to perform action in the component when user raise any event like clicking a button
 * Two Way Data Binding [()] uses combination of event binding and property binding to implement two way data binding
 * with the help of the directive ngModel
 */
@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent {

  pageTitle:string = "Data Binding in Angular";
  btnStatus: boolean = false;
  
  changeTitle(){
    this.pageTitle = "Data Binding";
  }

}
