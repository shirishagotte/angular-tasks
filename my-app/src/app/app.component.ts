import { AddItemAction, DeleteItemAction, LoadShoppingAction } from './store/actions/shopping-actions';
import { UsersService } from './services/users.service';
import { Component, OnInit } from '@angular/core';
import { AppState } from './store/models/app-state.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ShoppingItem } from './store/models/shopping-item.model';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
/**
 * In Angular, we can create a component by (ng generate component component-name OR ng g c component-name)
 * 1. Create a typescript class using keyword class followed by classname
 * 2. Attach metadata using @Component decorator 
 * 3. Import our component in AppModule and add it in declarations[] of @NgModule decorator
 * 
 * Finally, we can invoke the created component anywhere using the selector name which acts as custom html tag
 */
export class AppComponent implements OnInit{
  title = 'This is the Root Component (Parent Component)';

  //We can bind this variable username to the template using property binding
  //Then we can pass that property to the child component by using @Input() decorator
  username:string = "Parent's Data";
  childData:string;

  shoppingItems: Observable<Array<ShoppingItem>>;
  newShoppingItem: ShoppingItem = {id: '', name: ''};

  loading$: Observable<boolean>;
  error$: Observable<Error>;

  parentMethod(data){
    this.childData=data;
  }

  employees:any[]=[
    {name: 'Hari', empid:1001, gender:'male'},
    {name: 'Arun', empid:1002, gender:'male'},
    {name: 'Kavya', empid:1003, gender:'female'}    
  ]

  users:any[];
  myModel:any;

  gender:string;

  errorMessage:string='';

  testData: any[];
  constructor(private usersService:UsersService, private store: Store<AppState>){
   
  }

  ngOnInit(){
    this.usersService.getAllUsersFromApi().subscribe((data:any[])=>{
      this.users = data;
    },
    (err)=>{
      this.errorMessage=err.message;
    }
    );

    this.usersService.getAllUsersServiceFail().subscribe((data:any[])=>{
      this.testData=data;
    },
    (err)=>{
      this.errorMessage=err.message;
    }
    );

    this.shoppingItems = this.store.select(store=>store.shopping.list);
    this.loading$ = this.store.select(store=>store.shopping.loading);
    this.error$ = this.store.select(store=>store.shopping.error);

    this.store.dispatch(new LoadShoppingAction);
  }

  addItem() {
    this.newShoppingItem.id = uuid();
    this.store.dispatch(new AddItemAction(this.newShoppingItem));
    this.newShoppingItem = {id: '', name: ''};
  }

  deleteItem(id: string) {
    this.store.dispatch(new DeleteItemAction(id));
  }
  onBlurMethod(){
    alert(this.myModel) 
   }
}
