export class Employee {
    id              :   number;
    name            :   string;
    selected        :   boolean;
    courseSelected  :   string;
}
