// Section 1
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Tutorial } from './../models/tutorial.model'

// Section 2
export const ADD_TUTORIAL       = '[TUTORIAL] Add' //this is the type part of an action
export const REMOVE_TUTORIAL    = '[TUTORIAL] Remove' //this is the type part of an action

// Section 3
export class AddTutorial implements Action {
    readonly type = ADD_TUTORIAL

    //we don't even need a constructor if we do not need to pass any type of data
    //reducer takes the incoming action and decides what to do with it, 
    //it takes the previous state and returns a new state
    //based on the given action and the payload, i.e., the data that is coming
    constructor(public payload: Tutorial) {}
}

export class RemoveTutorial implements Action {
    readonly type = REMOVE_TUTORIAL

    constructor(public payload: number) {}
}

// Section 4
export type Actions = AddTutorial | RemoveTutorial //this will allow us to access our actions in the reducer