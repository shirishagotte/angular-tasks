export interface Tutorial {
    name: string;
    url: string;
    //an action in ngrx is 2 things
    //1. type : It is in the form of a string
        //It describes what is happening add tutorial, remove tutorial, update tutorial
    //2. payload : like name, url for tutorial
} 